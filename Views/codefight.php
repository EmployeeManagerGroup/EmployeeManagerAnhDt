<?php
function lateRide($n)
{

    $minute = $n % 60;
    $hour = ($n - $minute) / 60;

    $minute = str_pad($minute, 2, '0', STR_PAD_LEFT);
    $hour = str_pad($hour, 2, '0', STR_PAD_LEFT);

    return substr($minute, 0, 1) + substr($minute, -1) + substr($hour, 0, 1) + substr($hour, -1);


}

?>